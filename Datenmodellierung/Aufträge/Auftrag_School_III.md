![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "School III": 3. Normalform

Sie erstellen die 3. Normalform aufgrund Daten in der 2. Normalform

#### Aufgabe

Nehmen sie die Musterlösung von der Aufgabe **School II** und bringen sie diese in die 3. Normalform.

Tasks:

- Bringen sie die 2. Normalform in die 3. Normalform
- Fügen sie auch für die Entität *Lehrperson* Adressdaten ein. Eine Lehrperson wohnt an der gleichen Adresse wie ein Lehrnender. Ist ihre Lösung noch in 3. Normalform?

#### Zeit und Form

- 30 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162
