![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Social Media II": Vom konzeptionellen zum logischen ERD

In dieser Übung wandeln sie ein konzeptionellen in das logische ERD um. 

#### Aufgabe

Schauen sie sich das folgende konzeptionelle ERD an. Wandeln sie es in ein logisches um.

![konzERD](x_gitressourcen/SocialMedia_II.png)

Zusätzlich sollen die folgenden Kriterien erfüllt werden:

- Das Datum des Likes soll zusätzlich erfasst werden, so dass man später danach anzeigen, sortieren und filtern kann.
- Es soll gespeichert werden, wann sich der Benutzer angemeldet hat. 
- Ein Post besteht aus einem Text, optionalen Bild oder optionalen Video

**Tasks**:

- Erstellen sie das ERD mit einem Tool (z.B. draw.io)
- lösen sie netzwerkförmige Kardinalitäten auf
- Fügen sie Primärschlüssel und Fremdschlüssel hinzu
- Fügen sie weitere Attribute hinzu
- Lösen sie die genannten Kriterien

#### Zeit und Form

- 20 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162