![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# Open Data <!-- omit in toc -->

[TOC]

## Einführung

Bei Open Data handelt es sich um Bereitstellung von Daten, die offen zugänglich, verwertbar, bearbeitbar und von jedermann für beliebige Zwecke, auch kommerziell, geteilt werden können. Open Data wird unter einer offenen Lizenz lizenziert. Nicht alle Daten dürfen grundsätzlich frei verfügbar gemacht werden. Gestützt auf Erlasse (Datenschutz, Urheberrechte, Informationsschutz) dürfen sie nicht oder nur zu restriktiveren Bedingungen veröffentlicht werden. Eine weitere Einschränkung ist die Verpflichtung, den Urheber der Daten zu nennen.

Das [Open Data Handbook](http://opendatahandbook.org) erklärt, wieso man Daten veröffentlichen soll, was der Nutzen für uns ist und wie Open Data überhaupt geht.

Seit 2011 setzt sich die Organisation [opendata.ch](https://opendata.ch) für die Stärkung von Transparenz, Partizipation und Innovation in der Schweiz ein. Verschiedene Projekte konnten dadurch umgesetzt werden und dadurch die Menschen mit offenen Daten und offenem Wissen befähigen.

## Open Data Bewegung

Open Data ist nicht neu. Schon früh gab es solche Ideen, Daten weltweit für die Öffentlichkeit zur Verfügung zu stellen. Vor allem in der Wissenschaft merkte man schon in den 60/70er Jahren, dass das Teilen von wissenschaftlichen Daten zu wissenschaftlichen Entdeckungen und letztendlich zur Verbesserung der Forschung und somit auch zur Verbesserung der Bildung führte.

[NASA](https://www.nasa.gov) (National Aeronautics and Space Administration) und ihre Partner zum Beispiel haben Satellitendaten für die Wissenschaft offen gelegt. Spätestens mit World Wide Web wurden immer mehr und mehr Daten veröffentlicht und so u.a. die Open Data Bewegung in Gang gesetzt.

Mittlerweile hat sich die Open Data Bewegung ausgebreitet und weitere Bewegungen ausgelöst:

**Open Government (Veröffentlichung von Regierungs- und Verwaltungsdaten)**
Bundesamt für Statistik https://www.bfs.admin.ch/bfs/de/home.html
Open Data Stadt Zürich https://www.stadt-zuerich.ch/portal/de/index/ogd.html
Open Data Schweiz Register: https://opendata.swiss

**Open Science/ Open Access (Veröffentlichung Forschungsergebnisse/ offener Zugang zu wissenschaftlicher Literatur)**
Open Science UZH https://www.uzh.ch/de/researchinnovation/openscience.html
Open Access Network https://open-access.network/startseite

**Open Source (Veröffentlichung von Sourcecode)**
Bekannteste Open Source Bewegung https://github.com/

## Open Data Projekte

Nachfolgend einige auserwählte Projekte basierend auf Open Data:

* Denmark Public Toilet https://beta.findtoilet.dk/
* Flugradar https://www.flightradar24.com/
* Zugradar https://maps.vasile.ch/transit-sbb/
* Schiffsradar https://www.marinetraffic.com/

---

&copy;TBZ, 2022, Modul: m162
